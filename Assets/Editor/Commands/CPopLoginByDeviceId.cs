﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using LunarPlugin;

using Pop.Client;

[CCommand("pop-login-by-device-id")]
public class CPopLoginByDeviceId : CCommand {

	void Execute () {
        string popId = CVars.c_pop_id.Value;
        string deviceId = CVars.c_device_id.Value;
        string tokenId;
        PopClient pop = PopClientBuilder.Builder().Build();
        PrintIndent("-----------------------------------");
        PrintIndent("|  Pop Login by DeviceId (^_^)    |");
        PrintIndent("-----------------------------------");
        PrintIndent(" {0} : {1} ", CVars.c_pop_id.Value, CVars.c_device_id.Value);
        PrintIndent("-----------------------------------");
        PopLoginResponse response = pop.LoginPlayerByDeviceId(popId, deviceId);
        CVars.c_token_id.Value = response.TokenId;
        PrintIndent("{0}", response);
        pop.RegisterPlayerByDeviceId(deviceId);
        PrintIndent("-----------------------------------");
	}
}
