﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using LunarPlugin;

[CCommand("hello")]
public class CHelloCommand : CCommand {

    void Execute()
    {
        PrintIndent("Hello, Matilda (^_^)");
    }

    void Execute(string name)
    {
        PrintIndent("Hello, {0} (^_^)", name);
    }
}
