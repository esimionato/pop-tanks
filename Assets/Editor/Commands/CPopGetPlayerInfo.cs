﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using LunarPlugin;

using Pop.Client;

[CCommand("pop-get-player-info")]
public class CPopGetPlayerInfo : CCommand {

	void Execute () {
        string popId = CVars.c_pop_id.Value;
        string token_id = CVars.c_token_id.Value;
        PopClient pop = PopClientBuilder.Builder().Build();
        PrintIndent("-----------------------------------");
        PrintIndent("|  Pop Get Player Info   (^_^)    |");
        PrintIndent("-----------------------------------");
        PrintIndent(" {0} : {1} ", popId, token_id);
        PrintIndent("-----------------------------------");
        PopGetPlayerInfoResponse response = pop.GetPlayerInfo(popId);
        PrintIndent("{0}", JsonUtility.ToJson(response));
        PrintIndent("-----------------------------------");
	}
}
