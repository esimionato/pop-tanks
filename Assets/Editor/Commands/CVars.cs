﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using LunarPlugin;

[CVarContainer]
public static class CVars
{
    public static readonly CVar c_token_id = new CVar("c_token", "");
    public static readonly CVar c_pop_id = new CVar("c_pop_id", "");
    public static readonly CVar c_device_id = new CVar("c_device_id", "");
    public static readonly CVar c_player_data = new CVar("c_player_data", "");
}
