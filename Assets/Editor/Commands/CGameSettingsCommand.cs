﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using LunarPlugin;

using Tanks;

[CCommand("game-settings")]
public class CGameSettingsCommand : CCommand {

    void Execute() {
        PrintIndent(" {0} ", GameSettings.s_Instance);
    }

}
