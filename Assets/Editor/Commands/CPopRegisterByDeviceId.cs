﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using LunarPlugin;

using Tanks.Data;
using Pop.Client;

[CCommand("pop-signup-by-device-id")]
public class CPopSignUpByDeviceId : CCommand {

	void Execute () {
        PrintIndent("Pop Remote SignUp (^_^)");

        PrintIndent("--------------------");
        PopClient pop = PopClientBuilder.Builder().Build();
        string deviceId = pop.GenerateUUID();
        PrintIndent("UUID: {0}", deviceId);
        PopRegisterResponse response = pop.RegisterPlayerByDeviceId(deviceId);
        CVars.c_device_id.Value = response.DeviceId;
        CVars.c_pop_id.Value = response.PopId;
        PrintIndent("--------------------");
	}
}
