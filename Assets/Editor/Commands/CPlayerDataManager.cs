﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using LunarPlugin;

using Tanks.Data;

[CCommand("player-data")]
public class CPlayerDataManager : CCommand {

    void Execute()
    {
        PrintIndent(" {0} ", PlayerDataManager.s_Instance);
    }
}
