﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pop
{
    public enum Color
    {
        aqua,
        black,
        blue,
        brown,
        cyan,
        darkblue,
        fuchsia,
        green,
        grey,
        lightblue,
        lime,
        magenta,
        maroon,
        navy,
        olive,
        purple,
        red,
        silver,
        teal,
        white,
        yello
    }

    public enum Size
    {
        p = 8,
        h3 = 10,
        h1 = 12
    }
}
