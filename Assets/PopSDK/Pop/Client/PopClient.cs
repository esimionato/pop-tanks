﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;

using UnityEngine;

using CI.HttpClient;

namespace Pop.Client
{
    public static class PopSettings {
        public static string TitleId = "1234";
        public static string DefaultServerHost = "192.168.0.20:8080";
        //public static string DefaultServerHost = "p8080.msdev.cmd.com.ar";
    }

    public interface PopClient
    {
        PopRegisterResponse RegisterPlayer(PopRegisterRequest popRq);
        PopRegisterResponse RegisterPlayerByDeviceId(string deviceId);
        PopLoginResponse LoginPlayer(PopLoginRequest popRq);
        PopLoginResponse LoginPlayerByDeviceId(string popId, string deviceId);
        PopGetPlayerInfoResponse GetPlayerInfo(PopGetPlayerInfoRequest popId);
        PopGetPlayerInfoResponse GetPlayerInfo(string popId);

        string GenerateUUID();
    }

    public class PopHttp
    {
        public Dictionary<string, string> headers = new Dictionary<string, string>();

        public PopHttp() {
            headers.Add("Content-Type", "application/json;charset=UTF-8");
        }

        public string DoPost(string url, string data) {
            Debug.LogFormat("{0} : {1}", url, data);
            byte[] bjson = System.Text.Encoding.UTF8.GetBytes(data);
            WWW www = new WWW(url, bjson, headers);
            while (!www.isDone) { } //StartCoroutine("WaitforRequest", www, fn);
            if (www.error != null) {
                return www.error;
            }
            return www.text;
        }

        IEnumerator WaitforRequest(WWW www) {
            while (!www.isDone) { };
            DebugPanel.Log("http_done", "http", www.isDone);
            if (www.error == null) {
                DebugPanel.Log("http_text", "http", www.text.ToString());
            } else {
                DebugPanel.Log("http_error", "http", www.error.ToString());
            }
            yield return 0; //yield return www;
        }
    }


    public class PopClientImpl : PopClient
    {
        public string _titleId;
        public string _serverHost;
        public PopHttp _http;

        System.Random random = new System.Random();

        public string GenerateUUID()
        {
            DateTime epochStart = new System.DateTime(1970, 1, 1, 8, 0, 0,
                System.DateTimeKind.Utc);
            double timestamp = (System.DateTime.UtcNow - epochStart).TotalSeconds;
            string uniqueID = _titleId
                + "-" + string.Format("{0:X}", Convert.ToInt32(timestamp))
                + "-" + string.Format("{0:X}", Convert.ToInt32(Time.time * 1000000))
                + "-" + string.Format("{0:X}", random.Next(100000000));
            return uniqueID;
        }



        public string getApiUrl() {
            return String.Format("http://{0}/api/{1}/client/", _serverHost, _titleId);
        }

        public PopLoginResponse LoginPlayerByDeviceId(string popId, string deviceId)
        {
            Debug.LogFormat("{0}:{1}", popId, deviceId);
            return this.LoginPlayer(
                PopLoginRequest.Builder().PopId(popId).DeviceId(deviceId).Build());
        }

        public PopLoginResponse LoginPlayer(PopLoginRequest popRq)
        {
            string url = getApiUrl() + "login-with-device-id";
            string jRequest = JsonUtility.ToJson(popRq);
            string jResponse = _http.DoPost(url, jRequest);
            Debug.LogFormat("{0} | {1} | {2}", url, jRequest, jResponse);
            PopLoginResponse response = JsonUtility.FromJson<PopLoginResponse>(jResponse);
            _http.headers.Add("Authorization", " Bearer " + response.id_token);
            return response;
        }

        public PopRegisterResponse RegisterPlayerByDeviceId(string deviceId)
        {
            return this.RegisterPlayer(
                PopRegisterRequest.Builder().DeviceId(deviceId).Build());
        }

        public PopRegisterResponse RegisterPlayer(PopRegisterRequest popRq)
        {
            string url = getApiUrl() + "register-with-device-id";
            string jRequest = JsonUtility.ToJson(popRq);
            string jResponse = _http.DoPost(url, jRequest);
            Debug.LogFormat("{0} | {1} | {2}", url, jRequest, jResponse);
            PopRegisterResponse response = JsonUtility.FromJson<PopRegisterResponse>(jResponse);
            return response;
        }

        public PopGetPlayerInfoResponse GetPlayerInfo(PopGetPlayerInfoRequest popRq)
        {
            Debug.LogFormat("{0}", popRq.PopId);
            string url = getApiUrl() + "get-player-payload-info";
            string jRequest = JsonUtility.ToJson(popRq);
            string jResponse = _http.DoPost(url, jRequest);
            Debug.LogFormat("{0} | {1} | {2}", url, jRequest, jResponse);
            PopGetPlayerInfoResponse response = JsonUtility.FromJson<PopGetPlayerInfoResponse>(jResponse);
            return response;
        }

        public PopGetPlayerInfoResponse GetPlayerInfo(string popId)
        {
            return this.GetPlayerInfo(
                PopGetPlayerInfoRequest.Builder().PopId(popId).Build()
                );
        }

        public void setTitleId(string titleId)
        {
            this._titleId = titleId;
        }

        public void setServerHost(string serverHost)
        {
            this._serverHost = serverHost;
        }

        public void setHttpClient(PopHttp http)
        {
            this._http = http;
        }


    }



    public class PopClientBuilder
    {
        private string _titleId;
        private string _serverHost;

        public static PopClientBuilder Builder() {
            PopClientBuilder builder = new PopClientBuilder();
            builder._titleId = PopSettings.TitleId;
            builder._serverHost = PopSettings.DefaultServerHost;
            return builder;
        }

        public PopClientBuilder TitleId(string titleId)
        {
            _titleId = titleId;
            return this;
        }

        public PopClientBuilder ServerUrl(string serverUrl) {
            _serverHost = serverUrl;
            return this;
        }

        public PopClient Build()
        {
            PopClientImpl client = new PopClientImpl();
            if (_titleId == null) {
                throw new Exception("Title Id must be empty");
            }
            if (_serverHost == null)
            {
                throw new Exception("Server Host must be empty");
            }
            PopHttp http = new PopHttp();
            client.setHttpClient(http);
            client.setTitleId(_titleId);
            client.setServerHost(_serverHost);
            return client;
        }
    }

    [Serializable]
    public class PopRegisterResponse
    {
        public string pop_id;
        public string device_id;
        public bool newly_created;
        public string game_id;

        public string PopId { get { return pop_id; } }
        public string DeviceId { get { return device_id; } }
        public bool NewlyCreated { get { return newly_created; } }
        public string GameId { get { return game_id; } }

        public override string ToString() {
            return "\"PopRegisterResponse\": " + JsonUtility.ToJson(this, true);
        }

    }

    [Serializable]
    public class PopLoginResponse
    {
        public string id_token;

        public string TokenId { get { return id_token; } }

        public override string ToString() {
            return "\"PopLoginResponse\": " + JsonUtility.ToJson(this, true);
        }
    }

    [Serializable]
    public class PopRegisterRequest
    {
        public string deviceId;

        public static Buildor Builder()
        {
            return new Buildor();
        }

        public class Buildor {
            protected string _deviceId;
            public Buildor DeviceId(string deviceId)
            {
                this._deviceId = deviceId;
                return this;
            }
            public PopRegisterRequest Build()
            {
                PopRegisterRequest request = new PopRegisterRequest();
                request.deviceId = _deviceId;
                return request;
            }
        }
    }

    [Serializable]
    public class PopLoginRequest
    {
        public string deviceId;
        public string popId;

        public static Buildor Builder()
        {
            return new Buildor();
        }

        public class Buildor
        {
            private string deviceId;
            private string popId;

            public Buildor DeviceId(string deviceId) {
                this.deviceId = deviceId;
                return this;
            }

            public Buildor PopId(string popId) {
                this.popId = popId;
                return this;
            }

            public PopLoginRequest Build()
            {
                PopLoginRequest request = new PopLoginRequest();
                request.deviceId = deviceId;
                request.popId = popId;
                return request;
            }
        }
    }

    [Serializable]
    public class PopGetPlayerInfoResponse
    {
        public string pop_id;
        public string game_id;
        public string player_data;
    
        public string PopId {  get { return pop_id; } }
        public string GameId {  get { return game_id; } }
        public string PlayerData {  get { return player_data; } }

    }

    [Serializable]
    public class PopPlayerDataUpdateItem
    {
        public string key;
        public string value;

        public PopPlayerDataUpdateItem(string key, string value)
        {
            this.key = key;
            this.value = value;
        }
    }

    [Serializable]
    public class PopGetPlayerInfoRequest
    {
        public string popId;

        public string PopId { get { return popId; } set { popId = value; } }

        public List<PopPlayerDataUpdateItem> data = new List<PopPlayerDataUpdateItem>();

        public static Buildor Builder()
        {
            return new Buildor();
        }

        public class Buildor
        {
            public string popId;

            public Buildor PopId(string popId)
            {
                this.popId = popId;
                return this;
            }
            public PopGetPlayerInfoRequest Build()
            {
                PopGetPlayerInfoRequest request = new PopGetPlayerInfoRequest();
                request.PopId = popId;
                request.data.Add(new PopPlayerDataUpdateItem("k1", "v1"));
                request.data.Add(new PopPlayerDataUpdateItem("k2", "v2"));
                request.data.Add(new PopPlayerDataUpdateItem("k3", "v3"));
                request.data.Add(new PopPlayerDataUpdateItem("k4", "v4"));
                return request;
            }
        }
    }
}
