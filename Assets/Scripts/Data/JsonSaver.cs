using UnityEngine;
using System.IO;

namespace Tanks.Data
{
	/// <summary>
	/// Json implementation of data persistence
	/// </summary>
	public class JsonSaver : IDataSaver
	{

        [SerializeField]
		private static string s_FileFullPath;

		#if UNITY_EDITOR
        [SerializeField]
		private static readonly string s_Filename = "tanks_editor.sav";
		#else
        [SerializeField]
		private static readonly string s_Filename = "tanks.sav";
		#endif

		public static string GetSaveFilename()
		{

            //c/Users/{user}/AppData/LocalLow/Unity Technologies/{Tanks!!!}/{tanks_editor.sav}
			s_FileFullPath = string.Format("{0}/{1}", Application.persistentDataPath, s_Filename);
            return s_FileFullPath;
		}

		protected virtual StreamWriter GetWriteStream()
		{
			return new StreamWriter(new FileStream(GetSaveFilename(), FileMode.Create));
		}

		protected virtual StreamReader GetReadStream()
		{
			return new StreamReader(new FileStream(GetSaveFilename(), FileMode.Open));
		}

		/// <summary>
		/// Save the specified data store
		/// </summary>
		/// <param name="data">Data.</param>
		public void Save(DataStore data)
		{
            Debug.LogFormat("JsonSaver->Save({0})", data);
			string json = JsonUtility.ToJson(data);

			using (StreamWriter writer = GetWriteStream())
			{
				writer.Write(json);
			}
		}

		/// <summary>
		/// Load the specified data store
		/// </summary>
		/// <param name="data">Data.</param>
		public bool Load(DataStore data)
		{
            Debug.LogFormat("JsonSaver->Load({0})", data);
			string loadFilename = GetSaveFilename();

			if (File.Exists(loadFilename))
			{
				using (StreamReader reader = GetReadStream())
				{
					JsonUtility.FromJsonOverwrite(reader.ReadToEnd(), data);
				}

				return true;
            }

			return false;
		}

		/// <summary>
		/// Deletes the save file
		/// </summary>
		public void Delete()
		{
			File.Delete(GetSaveFilename());
		}
	}
}