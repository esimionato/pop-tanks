﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Tanks.Map
{
	/// <summary>
	/// Concrete implementation of multiplayer map list
	/// </summary>
    [Serializable]
	[CreateAssetMenu(fileName = "MapList", menuName = "Maps/Create List", order = 1)]
	public class MapList : MapListBase<MapDetails>
	{
	}
}