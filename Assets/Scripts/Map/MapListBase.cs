﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace Tanks.Map
{
	/// <summary>
	/// Map list base - provides indexer implementation. It is a scriptable object
	/// </summary>
    [Serializable]
	public abstract class MapListBase<T> : ScriptableObject where T: MapDetails
	{
		[SerializeField]
		public List<T> m_Details;

		/// <summary>
		/// Gets the <see cref="Tanks.Map.MapListBase"/> at the specified index.
		/// </summary>
		/// <param name="index">Index.</param>
		[SerializeField]
		public T this [int index]
		{
			get { return m_Details[index]; }
		}

		/// <summary>
		/// Number of elements in the list
		/// </summary>
		/// <value>The count.</value>
		[SerializeField]
		public int Count
		{
			get{ return m_Details.Count; }
		}
	}
}